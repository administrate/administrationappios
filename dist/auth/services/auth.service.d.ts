import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { User } from './../../users/interfaces/User';
export declare class AuthService {
    private usersServices;
    private jwtService;
    constructor(usersServices: UsersService, jwtService: JwtService);
    validateUser(email: string, password: string): unknown;
    generateJWT(user: User): {
        data: {
            accessToken: any;
            user: User;
        };
        respuesta: boolean;
    };
}
