import { Request } from 'express';
import { User } from './../../users/interfaces/User';
import { AuthService } from '../services/auth.service';
export declare class AuthController {
    private authService;
    constructor(authService: AuthService);
    login(req: Request): {
        data: {
            accessToken: any;
            user: User;
        };
        respuesta: boolean;
    };
}
