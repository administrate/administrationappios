import { PayloadToken } from "src/models/token.model";
declare const JwtStrategy_base: any;
export declare class JwtStrategy extends JwtStrategy_base {
    constructor();
    validate(payload: PayloadToken): PayloadToken;
}
export {};
