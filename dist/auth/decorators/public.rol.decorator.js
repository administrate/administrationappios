"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublicRol = exports.IS_ROL_PUBLIC_KEY = void 0;
const common_1 = require("@nestjs/common");
exports.IS_ROL_PUBLIC_KEY = 'isPublicRole';
const PublicRol = () => (0, common_1.SetMetadata)(exports.IS_ROL_PUBLIC_KEY, true);
exports.PublicRol = PublicRol;
//# sourceMappingURL=public.rol.decorator.js.map