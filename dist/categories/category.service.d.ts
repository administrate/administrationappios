import { Category } from './interfaces/Category';
import { Model } from 'mongoose';
export declare class CategoryService {
    private categoryModel;
    constructor(categoryModel: Model<Category>);
    getCategories(): Promise<{}>;
    createCategory(item: Category): Promise<{}>;
    deleteCategory(id: string): Promise<{}>;
}
