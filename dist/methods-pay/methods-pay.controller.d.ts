import { MethodPay } from './interfaces/MethodPay';
import { MethodsPayService } from './methods-pay.service';
export declare class MethodsPayController {
    private methodPayService;
    constructor(methodPayService: MethodsPayService);
    getMethodsPay(): {};
    getMethodPay(id: any): {};
    createMethodPay(data: MethodPay): {};
    deleteMethodPay(id: any): {};
}
