"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.MethodsPayService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
let MethodsPayService = class MethodsPayService {
    constructor(methosPayModel) {
        this.methosPayModel = methosPayModel;
    }
    async getMethodsPay() {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /methos-pay/");
        console.log("data --> NA");
        try {
            const response = await this.methosPayModel.find();
            console.log("response --> ", response);
            return { data: response, respuesta: true };
        }
        catch (error) {
            console.log("response --> ", error);
            return { error: "Petición no válida. " + error, respuesta: false };
        }
    }
    async getMethodPay(id) {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /methos-pay/:id");
        console.log("data --> ", id);
        try {
            const response = await this.methosPayModel.findById(id);
            console.log("response->", response);
            if (response && response != null) {
                return { data: response, respuesta: true };
            }
            else {
                return { error: "Metodo de pago no existente", respuesta: false };
            }
        }
        catch (error) {
            console.log("response --> ", error);
            return { error: "Petición no válida. " + error, respuesta: false };
        }
    }
    async createMethodPay(item) {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /registers/create");
        console.log("data --> ", item);
        if (!('description' in item) || item.description === "") {
            return { respuesta: false, error: "faltan parametros" };
        }
        try {
            const newObject = new this.methosPayModel(item);
            const response = await newObject.save();
            console.log("response->", response);
            if (response) {
                return { data: response, respuesta: true };
            }
            else {
                return { error: "Metodo de pago no creado", respuesta: false };
            }
        }
        catch (error) {
            console.log("Response --> ", error);
            return { error: "Petición no válida. " + error, respuesta: false };
        }
    }
    async deleteMethodPay(id) {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /registers/update");
        console.log("data --> ", id);
        if (!id || id === "") {
            return { error: "Faltan datos", respuesta: false };
        }
        try {
            const deleteItem = await this.methosPayModel.findByIdAndDelete(id);
            console.log("response --> ", deleteItem);
            if (deleteItem && deleteItem != null) {
                return { data: deleteItem, respuesta: true };
            }
            else {
                return { error: "Registro no válido", respuesta: false };
            }
        }
        catch (error) {
            console.log("Response --> ", error);
            return { error: "Petición no válida. " + error, respuesta: false };
        }
    }
};
MethodsPayService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, mongoose_1.InjectModel)('MethosPay')),
    __metadata("design:paramtypes", [typeof (_a = typeof mongoose_2.Model !== "undefined" && mongoose_2.Model) === "function" ? _a : Object])
], MethodsPayService);
exports.MethodsPayService = MethodsPayService;
//# sourceMappingURL=methods-pay.service.js.map