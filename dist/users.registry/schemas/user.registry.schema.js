"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRegistrySchema = void 0;
const mongoose_1 = require("mongoose");
exports.UserRegistrySchema = new mongoose_1.Schema({
    token: String,
    idUser: Number,
    isActive: Boolean,
    count: Number,
    createAt: {
        type: Date,
        default: Date.now
    },
    updateAt: {
        type: Date,
        default: Date.now
    }
});
//# sourceMappingURL=user.registry.schema.js.map