export declare class CreateUserRegistryDto {
    token: string;
    idUser: string;
    isActive?: boolean;
    count?: number;
}
