import { Document } from "mongoose";
export interface UserRegistry extends Document {
    _id?: number;
    token: string;
    idUser: string;
    isActive: boolean;
    count: number;
}
