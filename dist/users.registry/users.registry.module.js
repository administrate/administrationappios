"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersRegistryModule = void 0;
const common_1 = require("@nestjs/common");
const users_registry_service_1 = require("./users.registry.service");
const users_registry_controller_1 = require("./users.registry.controller");
const mongoose_1 = require("@nestjs/mongoose");
const user_registry_schema_1 = require("./schemas/user.registry.schema");
const SchemasModules = mongoose_1.MongooseModule.forFeature([
    { name: 'UserRegistry', schema: user_registry_schema_1.UserRegistrySchema }
]);
let UsersRegistryModule = class UsersRegistryModule {
};
UsersRegistryModule = __decorate([
    (0, common_1.Module)({
        imports: [SchemasModules],
        providers: [users_registry_service_1.UsersRegistryService],
        controllers: [users_registry_controller_1.UsersRegistryController],
    })
], UsersRegistryModule);
exports.UsersRegistryModule = UsersRegistryModule;
//# sourceMappingURL=users.registry.module.js.map