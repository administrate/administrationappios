import { CreateUserDto } from './dto/CreateUserDto';
import { UsersService } from './users.service';
export declare class UsersController {
    private usersService;
    constructor(usersService: UsersService);
    getUsers(): {};
    getUser(id: any): {};
    createUser(data: CreateUserDto): {};
    createAdmin(data: CreateUserDto): {};
    updateStatusUser(data: CreateUserDto, id: any): {};
    deleteUser(id: any): {};
}
