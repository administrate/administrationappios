"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const bcrypt = require("bcrypt");
let UsersService = class UsersService {
    constructor(userModel) {
        this.userModel = userModel;
    }
    async getUsers() {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /users/");
        console.log("data --> NA");
        try {
            const response = await this.userModel.find();
            response.forEach(it => {
                it.password = undefined;
            });
            console.log("response --> ", response);
            return { data: response, respuesta: true };
        }
        catch (error) {
            console.log("response --> ", error);
            return { error: "Petición no válida. " + error, respuesta: false };
        }
    }
    async getUser(id) {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /users/:id");
        console.log("data --> ", id);
        try {
            const response = await this.userModel.findById(id);
            console.log("response->", response);
            if (response && response != null) {
                const _a = response.toJSON(), { password } = _a, rts = __rest(_a, ["password"]);
                return { data: rts, respuesta: true };
            }
            else {
                return { error: "Usuario no registrado", respuesta: false };
            }
        }
        catch (error) {
            console.log("response --> ", error);
            return { error: "Usuario no válido. " + error, respuesta: false };
        }
    }
    async getUserByMail(email) {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /users/getUserByMail");
        console.log("data --> ", email);
        try {
            const response = await this.userModel.findOne({ email: email }).exec();
            console.log("response->", response);
            if (response && response != null) {
                return { data: response, respuesta: true };
            }
            else {
                return { error: "Usuario no registrado", respuesta: false };
            }
        }
        catch (error) {
            console.log("response --> ", error);
            return { error: "Usuario no válido. " + error, respuesta: false };
        }
    }
    async createUser(data) {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /users/Create");
        console.log("data --> ", data);
        try {
            if (!('email' in data) || !('password' in data) || !('name' in data) || !('phone' in data) ||
                data.email == "" || data.password == "" || data.name == "" || data.phone == "") {
                return { error: "Faltan datos", respuesta: false };
            }
            const isUserExist = await this.getUserByMail(data.email);
            console.log("response getByEmail->", isUserExist.respuesta);
            if (isUserExist.respuesta)
                return { error: "Ya existe un usuario con ese correo", respuesta: false };
            const bCryptPass = await bcrypt.hash(data.password, 10);
            const newObject = new this.userModel(data);
            newObject.password = bCryptPass;
            newObject.status = 1;
            newObject.type = 1;
            const response = await newObject.save();
            console.log("response->", response);
            if (response) {
                const _a = response.toJSON(), { password } = _a, rta = __rest(_a, ["password"]);
                return { data: rta, respuesta: true };
            }
            else {
                return { error: "Usuario no registrado", respuesta: false };
            }
        }
        catch (error) {
            console.log("Response --> ", error);
            return { error: "Usuario no válido. " + error, respuesta: false };
        }
    }
    async deleteUser(id) {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /users/delete");
        console.log("data --> ", id);
        try {
            if (!id && id == "") {
                return {};
            }
            const query = await this.userModel.findByIdAndDelete(id);
            if (query) {
                return { data: query, respuesta: true };
            }
            else {
                return { error: 'Usuario no se pudo eliminar.', respuesta: false };
            }
        }
        catch (error) {
            console.log("Response --> ", error);
            return { error: "Usuario no válido. " + error, respuesta: false };
        }
    }
    async createAdmin(data) {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /users/Create");
        console.log("data --> ", data);
        try {
            if (!('email' in data) || !('password' in data) || !('name' in data) || !('phone' in data) ||
                data.email == "" || data.password == "" || data.name == "" || data.phone == "") {
                return { error: "Faltan datos", respuesta: false };
            }
            const isUserExist = await this.getUserByMail(data.email);
            console.log("response getByEmail->", isUserExist.respuesta);
            if (isUserExist.respuesta)
                return { error: "Ya existe un usuario con ese correo", respuesta: false };
            const bCryptPass = await bcrypt.hash(data.password, 10);
            const newObject = new this.userModel(data);
            newObject.password = bCryptPass;
            newObject.type = 0;
            newObject.status = 1;
            const response = await newObject.save();
            console.log("response->", response);
            if (response) {
                const _a = response.toJSON(), { password } = _a, rta = __rest(_a, ["password"]);
                return { data: rta, respuesta: true };
            }
            else {
                return { error: "Usuario no registrado", respuesta: false };
            }
        }
        catch (error) {
            console.log("Response --> ", error);
            return { error: "Usuario no válido. " + error, respuesta: false };
        }
    }
    async updateUser(data, id) {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /users/:id");
        console.log("id --> ", id);
        console.log("data --> ", data);
        try {
            if (id) {
                const user = await this.userModel.findById(id);
                if (user) {
                    console.log("USERRES --->", user);
                    if ('status' in data) {
                        user.status = data.status;
                    }
                    if ('email' in data) {
                        user.email = data.email;
                    }
                    if ('password' in data) {
                        user.password = data.password;
                    }
                    if ('name' in data) {
                        user.name = data.name;
                    }
                    if ('type' in data) {
                        user.type = data.type;
                    }
                    if ('phone' in data) {
                        user.phone = data.phone;
                    }
                    if ('employeeCode' in data) {
                        user.employeeCode = data.employeeCode;
                    }
                    if ('bussinesUnityId' in data) {
                        user.bussinesUnityId = data.bussinesUnityId;
                    }
                    console.log("result EDITED", user);
                    const response = await user.save();
                    if (response) {
                        return { respuesta: true, data: response };
                    }
                    else {
                        return { respuesta: false, error: 'No se pudo actualizar el usuario' };
                    }
                }
                else {
                    return { respuesta: false, error: 'Usuario no existente.' };
                }
            }
        }
        catch (err) {
            return { respuesta: false, error: 'Error inesperado. [2000]' + err };
        }
    }
};
UsersService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, mongoose_1.InjectModel)('User')),
    __metadata("design:paramtypes", [typeof (_a = typeof mongoose_2.Model !== "undefined" && mongoose_2.Model) === "function" ? _a : Object])
], UsersService);
exports.UsersService = UsersService;
//# sourceMappingURL=users.service.js.map