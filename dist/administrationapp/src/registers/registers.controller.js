"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RegistersController = void 0;
const common_1 = require("@nestjs/common");
const CreateRegisterDto_1 = require("./dto/CreateRegisterDto");
const registers_service_1 = require("./registers.service");
const jwt_auth_guard_1 = require("../../../src/auth/guards/jwt-auth.guard");
let RegistersController = class RegistersController {
    constructor(registerService) {
        this.registerService = registerService;
    }
    getRegisters() {
        return this.registerService.getRegisters();
    }
    getRegister(id) {
        return this.registerService.getRegister(id);
    }
    getRegisterByUSer(id) {
        return this.registerService.getRegistersByAccount(id);
    }
    getGraphsByUSer(id, filter) {
        return this.registerService.getAnalithyscsByAccount(id, filter);
    }
    createRegister(data) {
        return this.registerService.createRegister(data);
    }
    deleteRegisters(id) {
        return this.registerService.deleteRegister(id);
    }
};
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Object)
], RegistersController.prototype, "getRegisters", null);
__decorate([
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Object)
], RegistersController.prototype, "getRegister", null);
__decorate([
    (0, common_1.Get)('byAccount/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Object)
], RegistersController.prototype, "getRegisterByUSer", null);
__decorate([
    (0, common_1.Get)('graphs/:id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Query)('filter')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Object)
], RegistersController.prototype, "getGraphsByUSer", null);
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [CreateRegisterDto_1.CreateRegisterDto]),
    __metadata("design:returntype", Object)
], RegistersController.prototype, "createRegister", null);
__decorate([
    (0, common_1.Delete)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Object)
], RegistersController.prototype, "deleteRegisters", null);
RegistersController = __decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Controller)('registers'),
    __metadata("design:paramtypes", [registers_service_1.RegistersService])
], RegistersController);
exports.RegistersController = RegistersController;
//# sourceMappingURL=registers.controller.js.map