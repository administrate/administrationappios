export interface Register {
    id?: number;
    name: string;
    observations?: string;
    quantity?: number;
    typeMovement: number;
    price: number;
    ticket?: string;
    place?: string;
    payTypeId: string;
    tips?: number;
    idStatus?: string;
    idCategory?: string;
    idAccount: string;
}
