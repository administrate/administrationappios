"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RegisterSchema = void 0;
const mongoose_1 = require("mongoose");
exports.RegisterSchema = new mongoose_1.Schema({
    name: String,
    observations: String,
    quantity: Number,
    typeMovement: Number,
    price: Number,
    ticket: String,
    place: String,
    payTypeId: {
        required: false,
        type: mongoose_1.Schema.Types.ObjectId,
        default: null
    },
    tips: Number,
    idStatus: {
        required: false,
        type: mongoose_1.Schema.Types.ObjectId,
        default: null
    },
    createAt: {
        type: Date,
        default: Date.now()
    },
    updateAt: {
        type: Date,
        default: Date.now()
    },
    idAccount: {
        required: false,
        type: mongoose_1.Schema.Types.ObjectId,
        default: null
    },
    idCategory: {
        required: false,
        type: mongoose_1.Schema.Types.ObjectId,
        default: null
    }
});
//# sourceMappingURL=register.schema.js.map