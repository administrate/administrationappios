export declare class CreateRegisterDto {
    name: string;
    observations?: string;
    quantity?: number;
    typeMovement: number;
    price: number;
    ticket?: string;
    place?: string;
    payTypeId: string;
    tips?: number;
    idCategory?: string;
    idStatus?: string;
    idAccount: string;
    createAt?: string;
}
