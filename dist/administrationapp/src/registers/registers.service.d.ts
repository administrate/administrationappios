import { Register } from './interfaces/Register';
import { Model } from 'mongoose';
import { CreateRegisterDto } from './dto/CreateRegisterDto';
import { CategoryService } from 'src/categories/category.service';
import { AccountService } from 'src/acounts/acount.service';
export declare class RegistersService {
    private registerModel;
    private categoryServices;
    private accountServices;
    constructor(registerModel: Model<Register>, categoryServices: CategoryService, accountServices: AccountService);
    getRegisters(): Promise<{}>;
    getRegister(id: any): Promise<{}>;
    getAnalithyscsByAccount(id: any, filter: any): Promise<{}>;
    getRegistersByAccount(id: any): Promise<{}>;
    createRegister(data: CreateRegisterDto): Promise<{}>;
    deleteRegister(id: string): Promise<{}>;
    getStrDate(dateToParse: Date): String;
    getDateMonth(formateDte: String): {};
    byDate(a: any, b: any): number;
}
