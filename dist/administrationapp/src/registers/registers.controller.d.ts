import { CreateRegisterDto } from './dto/CreateRegisterDto';
import { RegistersService } from './registers.service';
export declare class RegistersController {
    private registerService;
    constructor(registerService: RegistersService);
    getRegisters(): {};
    getRegister(id: any): {};
    getRegisterByUSer(id: any): {};
    getGraphsByUSer(id: any, filter: any): {};
    createRegister(data: CreateRegisterDto): {};
    deleteRegisters(id: any): {};
}
