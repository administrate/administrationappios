import { Document } from "mongoose";
export interface User extends Document {
    _id?: string;
    email: string;
    type: number;
    password: string;
    name: string;
    phone: string;
    status: number;
    employeeCode?: string;
    bussinesUnityId?: string;
}
