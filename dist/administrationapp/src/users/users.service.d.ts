import { Model } from 'mongoose';
import { User } from './interfaces/User';
import { CreateUserDto } from './dto/CreateUserDto';
export declare class UsersService {
    private userModel;
    constructor(userModel: Model<User>);
    getUsers(): Promise<{}>;
    getUser(id: any): Promise<{}>;
    getUserByMail(email: string): Promise<{
        respuesta: boolean;
        data?: User;
        error?: string;
    }>;
    createUser(data: CreateUserDto): Promise<{}>;
    deleteUser(id: string): Promise<{}>;
    createAdmin(data: CreateUserDto): Promise<{}>;
    updateUser(data: CreateUserDto, id: string): Promise<{}>;
}
