"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserSchema = void 0;
const mongoose_1 = require("mongoose");
exports.UserSchema = new mongoose_1.Schema({
    email: String,
    type: Number,
    password: String,
    name: String,
    phone: String,
    employeeCode: String,
    bussinesUnityId: String,
    status: Number,
    createAt: {
        type: Date,
        default: Date.now
    },
    updateAt: {
        type: Date,
        default: Date.now
    }
});
//# sourceMappingURL=user.schema.js.map