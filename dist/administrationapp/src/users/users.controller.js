"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersController = void 0;
const common_1 = require("@nestjs/common");
const CreateUserDto_1 = require("./dto/CreateUserDto");
const users_service_1 = require("./users.service");
const jwt_auth_guard_1 = require("../../../src/auth/guards/jwt-auth.guard");
const public_decorator_1 = require("../../../src/auth/decorators/public.decorator");
const roles_decorator_1 = require("../../../src/auth/decorators/roles.decorator");
const rol_model_1 = require("../../../src/models/rol.model");
const roles_auth_guard_1 = require("../../../src/auth/guards/roles-auth.guard");
let UsersController = class UsersController {
    constructor(usersService) {
        this.usersService = usersService;
    }
    getUsers() {
        return this.usersService.getUsers();
    }
    getUser(id) {
        return this.usersService.getUser(id);
    }
    createUser(data) {
        return this.usersService.createUser(data);
    }
    createAdmin(data) {
        return this.usersService.createAdmin(data);
    }
    updateStatusUser(data, id) {
        return this.usersService.updateUser(data, id);
    }
    deleteUser(id) {
        return this.usersService.deleteUser(id);
    }
};
__decorate([
    (0, roles_decorator_1.Roles)(rol_model_1.Role.ADMIN),
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Object)
], UsersController.prototype, "getUsers", null);
__decorate([
    (0, roles_decorator_1.Roles)(rol_model_1.Role.ADMIN),
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Object)
], UsersController.prototype, "getUser", null);
__decorate([
    (0, public_decorator_1.Public)(),
    (0, common_1.Post)('create'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [CreateUserDto_1.CreateUserDto]),
    __metadata("design:returntype", Object)
], UsersController.prototype, "createUser", null);
__decorate([
    (0, roles_decorator_1.Roles)(rol_model_1.Role.ADMIN),
    (0, common_1.Post)('createAdmin'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [CreateUserDto_1.CreateUserDto]),
    __metadata("design:returntype", Object)
], UsersController.prototype, "createAdmin", null);
__decorate([
    (0, roles_decorator_1.Roles)(rol_model_1.Role.ADMIN),
    (0, common_1.Put)(':id'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [CreateUserDto_1.CreateUserDto, Object]),
    __metadata("design:returntype", Object)
], UsersController.prototype, "updateStatusUser", null);
__decorate([
    (0, roles_decorator_1.Roles)(rol_model_1.Role.ADMIN),
    (0, common_1.Delete)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Object)
], UsersController.prototype, "deleteUser", null);
UsersController = __decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard, roles_auth_guard_1.RolesAuthGuard),
    (0, common_1.Controller)('users'),
    __metadata("design:paramtypes", [users_service_1.UsersService])
], UsersController);
exports.UsersController = UsersController;
//# sourceMappingURL=users.controller.js.map