export declare class CreateUserDto {
    email: string;
    password: string;
    name: string;
    phone: string;
    status: number;
    type: number;
    employeeCode?: string;
    bussinesUnityId?: string;
}
