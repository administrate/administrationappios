"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountSchema = void 0;
const mongoose_1 = require("mongoose");
exports.AccountSchema = new mongoose_1.Schema({
    balance: Number,
    idUser: {
        required: false,
        type: mongoose_1.Schema.Types.ObjectId,
        default: null
    },
    name: String,
    createAt: {
        type: Date,
        default: Date.now
    },
    updateAt: {
        type: Date,
        default: Date.now
    }
});
//# sourceMappingURL=account.schemas.js.map