import { Account } from './interfaces/Account';
import { AccountService } from './acount.service';
import { CreateAccountDto } from './dto/CreateAccountDto';
export declare class AccountController {
    private accountService;
    constructor(accountService: AccountService);
    getAccounts(): {};
    getAccount(id: any): {};
    getAccountsByUser(id: any): {};
    createAccount(data: Account): {};
    deleteAccount(id: any): {};
    updateAccount(id: any, balance: CreateAccountDto): {};
}
