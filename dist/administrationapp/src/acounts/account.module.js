"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountsModule = void 0;
const common_1 = require("@nestjs/common");
const acount_service_1 = require("./acount.service");
const acount_controller_1 = require("./acount.controller");
const mongoose_1 = require("@nestjs/mongoose");
const account_schemas_1 = require("./schema/account.schemas");
const SchemasModules = mongoose_1.MongooseModule.forFeature([
    { name: 'Account', schema: account_schemas_1.AccountSchema }
]);
let AccountsModule = class AccountsModule {
};
AccountsModule = __decorate([
    (0, common_1.Module)({
        providers: [acount_service_1.AccountService],
        imports: [SchemasModules],
        controllers: [acount_controller_1.AccountController],
        exports: [acount_service_1.AccountService],
    })
], AccountsModule);
exports.AccountsModule = AccountsModule;
//# sourceMappingURL=account.module.js.map