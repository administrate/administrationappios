"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountController = void 0;
const common_1 = require("@nestjs/common");
const acount_service_1 = require("./acount.service");
const jwt_auth_guard_1 = require("../../../src/auth/guards/jwt-auth.guard");
const CreateAccountDto_1 = require("./dto/CreateAccountDto");
let AccountController = class AccountController {
    constructor(accountService) {
        this.accountService = accountService;
    }
    getAccounts() {
        return this.accountService.getAccounts();
    }
    getAccount(id) {
        return this.accountService.getAccount(id);
    }
    getAccountsByUser(id) {
        return this.accountService.getAccountsByUser(id);
    }
    createAccount(data) {
        return this.accountService.createAccount(data);
    }
    deleteAccount(id) {
        return this.accountService.deleteAccount(id);
    }
    updateAccount(id, balance) {
        return this.accountService.updateAccount(balance, id);
    }
};
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Object)
], AccountController.prototype, "getAccounts", null);
__decorate([
    (0, common_1.Get)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Object)
], AccountController.prototype, "getAccount", null);
__decorate([
    (0, common_1.Get)('byUser/:id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Object)
], AccountController.prototype, "getAccountsByUser", null);
__decorate([
    (0, common_1.Post)('create'),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Object)
], AccountController.prototype, "createAccount", null);
__decorate([
    (0, common_1.Delete)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Object)
], AccountController.prototype, "deleteAccount", null);
__decorate([
    (0, common_1.Put)(':id'),
    __param(0, (0, common_1.Param)('id')),
    __param(1, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, CreateAccountDto_1.CreateAccountDto]),
    __metadata("design:returntype", Object)
], AccountController.prototype, "updateAccount", null);
AccountController = __decorate([
    (0, common_1.UseGuards)(jwt_auth_guard_1.JwtAuthGuard),
    (0, common_1.Controller)('account'),
    __metadata("design:paramtypes", [acount_service_1.AccountService])
], AccountController);
exports.AccountController = AccountController;
//# sourceMappingURL=acount.controller.js.map