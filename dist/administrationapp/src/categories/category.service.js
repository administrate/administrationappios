"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CategoryService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
let CategoryService = class CategoryService {
    constructor(categoryModel) {
        this.categoryModel = categoryModel;
    }
    async getCategories() {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /categories/");
        console.log("data --> NA");
        try {
            const response = await this.categoryModel.find();
            console.log("response --> ", response);
            return { data: response, respuesta: true };
        }
        catch (error) {
            console.log("response --> ", error);
            return { error: "Petición no válida. " + error, respuesta: false };
        }
    }
    async createCategory(item) {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /categories/create");
        console.log("data --> ", item);
        if (!('description' in item) || item.description === "") {
            return { respuesta: false, error: "faltan parametros" };
        }
        try {
            const newObject = new this.categoryModel(item);
            const response = await newObject.save();
            console.log("response->", response);
            if (response) {
                return { data: response, respuesta: true };
            }
            else {
                return { error: "Categoria no creada", respuesta: false };
            }
        }
        catch (error) {
            console.log("Response --> ", error);
            return { error: "Petición no válida. " + error, respuesta: false };
        }
    }
    async deleteCategory(id) {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /categories/update");
        console.log("data --> ", id);
        if (!id || id === "") {
            return { error: "Faltan datos", respuesta: false };
        }
        try {
            const deleteItem = await this.categoryModel.findByIdAndDelete(id);
            console.log("response --> ", deleteItem);
            if (deleteItem && deleteItem != null) {
                return { data: deleteItem, respuesta: true };
            }
            else {
                return { error: "Registro no válido", respuesta: false };
            }
        }
        catch (error) {
            console.log("Response --> ", error);
            return { error: "Petición no válida. " + error, respuesta: false };
        }
    }
};
CategoryService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, mongoose_1.InjectModel)('Category')),
    __metadata("design:paramtypes", [mongoose_2.Model])
], CategoryService);
exports.CategoryService = CategoryService;
//# sourceMappingURL=category.service.js.map