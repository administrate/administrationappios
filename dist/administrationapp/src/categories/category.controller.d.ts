import { CategoryService } from './category.service';
import { Category } from './interfaces/Category';
export declare class CategoryController {
    private methodPayService;
    constructor(methodPayService: CategoryService);
    getMethodsPay(): {};
    createMethodPay(data: Category): {};
    deleteMethodPay(id: any): {};
}
