export interface Category {
    id?: string;
    description: string;
    data: string;
}
