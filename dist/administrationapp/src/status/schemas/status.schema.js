"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.StatusSchema = void 0;
const mongoose_1 = require("mongoose");
exports.StatusSchema = new mongoose_1.Schema({
    description: String,
    data: String,
    createAt: {
        type: Date,
        default: Date.now
    },
    updateAt: {
        type: Date,
        default: Date.now
    }
});
//# sourceMappingURL=status.schema.js.map