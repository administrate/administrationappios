import { Status } from './interfaces/Status';
import { Model } from 'mongoose';
export declare class StatusService {
    private statusModel;
    constructor(statusModel: Model<Status>);
    getStatus(): Promise<{}>;
    createStatus(item: Status): Promise<{}>;
    deleteStatus(id: string): Promise<{}>;
}
