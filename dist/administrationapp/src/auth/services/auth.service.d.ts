import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import { User } from './../../users/interfaces/User';
export declare class AuthService {
    private usersServices;
    private jwtService;
    constructor(usersServices: UsersService, jwtService: JwtService);
    validateUser(email: string, password: string): Promise<{
        id?: any;
        name: string;
        _id?: string;
        __v?: any;
        type: number;
        email: string;
        phone: string;
        status: number;
        employeeCode?: string;
        bussinesUnityId?: string;
    }>;
    generateJWT(user: User): {
        data: {
            accessToken: string;
            user: User;
        };
        respuesta: boolean;
    };
}
