import { Strategy } from "passport-local";
import { AuthService } from "../services/auth.service";
declare const LocalStrategy_base: new (...args: any[]) => Strategy;
export declare class LocalStrategy extends LocalStrategy_base {
    private authService;
    constructor(authService: AuthService);
    validate(email: string, password: string): Promise<{
        id?: any;
        name: string;
        _id?: string;
        __v?: any;
        type: number;
        email: string;
        phone: string;
        status: number;
        employeeCode?: string;
        bussinesUnityId?: string;
    }>;
}
export {};
