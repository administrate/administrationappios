"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MethodPaySchema = void 0;
const mongoose_1 = require("mongoose");
exports.MethodPaySchema = new mongoose_1.Schema({
    description: String,
    data: String,
    createAt: {
        type: Date,
        default: Date.now
    },
    updateAt: {
        type: Date,
        default: Date.now
    }
});
//# sourceMappingURL=methos-pay.schemas.js.map