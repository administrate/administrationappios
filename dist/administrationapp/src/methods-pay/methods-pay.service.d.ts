import { MethodPay } from './interfaces/MethodPay';
import { Model } from 'mongoose';
export declare class MethodsPayService {
    private methosPayModel;
    constructor(methosPayModel: Model<MethodPay>);
    getMethodsPay(): Promise<{}>;
    getMethodPay(id: string): Promise<{}>;
    createMethodPay(item: MethodPay): Promise<{}>;
    deleteMethodPay(id: string): Promise<{}>;
}
