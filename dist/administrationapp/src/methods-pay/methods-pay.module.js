"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MethodsPayModule = void 0;
const common_1 = require("@nestjs/common");
const methods_pay_service_1 = require("./methods-pay.service");
const methods_pay_controller_1 = require("./methods-pay.controller");
const mongoose_1 = require("@nestjs/mongoose");
const methos_pay_schemas_1 = require("./schemas/methos-pay.schemas");
const SchemasModules = mongoose_1.MongooseModule.forFeature([
    { name: 'MethosPay', schema: methos_pay_schemas_1.MethodPaySchema }
]);
let MethodsPayModule = class MethodsPayModule {
};
MethodsPayModule = __decorate([
    (0, common_1.Module)({
        providers: [methods_pay_service_1.MethodsPayService],
        imports: [SchemasModules],
        controllers: [methods_pay_controller_1.MethodsPayController]
    })
], MethodsPayModule);
exports.MethodsPayModule = MethodsPayModule;
//# sourceMappingURL=methods-pay.module.js.map