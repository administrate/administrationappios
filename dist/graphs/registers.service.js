"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RegistersService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
const category_service_1 = require("../categories/category.service");
const acount_service_1 = require("../acounts/acount.service");
const d3 = require("d3-collection");
let mongooe = require('mongoose');
let RegistersService = class RegistersService {
    constructor(registerModel, categoryServices, accountServices) {
        this.registerModel = registerModel;
        this.categoryServices = categoryServices;
        this.accountServices = accountServices;
    }
    async getRegisters() {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /registers/");
        console.log("data --> NA");
        try {
            const response = await this.registerModel.find();
            console.log("response --> ", response);
            return { data: response, respuesta: true };
        }
        catch (error) {
            console.log("response --> ", error);
            return { error: "Petición no válida. " + error, respuesta: false };
        }
    }
    async getRegister(id) {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /registers/:id");
        console.log("data --> ", id);
        try {
            const response = await this.registerModel.findById(id);
            console.log("response->", response);
            if (response && response != null) {
                return { data: response, respuesta: true };
            }
            else {
                return { error: "Registro no existente", respuesta: false };
            }
        }
        catch (error) {
            console.log("response --> ", error);
            return { error: "Petición no válida. " + error, respuesta: false };
        }
    }
    async getRegistersByAccount(id) {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /getRegisterById/:id");
        console.log("data --> ", id);
        try {
            let dte = this.getStrDate(new Date());
            let startToendMonth = this.getDateMonth(dte);
            const response = await this.registerModel.aggregate([
                { $addFields: { stringDate: { $dateToString: { format: "%Y-%m-%d", date: "$createAt" } } } },
                { $match: { idAccount: mongooe.Types.ObjectId(id), "stringDate": startToendMonth } },
            ]);
            var expensesByDate = d3.nest()
                .key(function (d) { return d.createAt; }).sortKeys(d3.ascending)
                .entries(response);
            let arrayinverted = expensesByDate.sort(this.byDate);
            return { data: arrayinverted, respuesta: true };
        }
        catch (error) {
            return { error: "Petición no válida. " + error, respuesta: false };
        }
    }
    async createRegister(data) {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /registers/create");
        console.log("data --> ", data);
        if (!('price' in data) || !('idCategory' in data) || !('idAccount' in data) || !('name' in data) ||
            data.price < 0 || data.name == "") {
            throw new common_1.HttpException('Faltan algunos datos obligatorios, favor de verificar.', common_1.HttpStatus.BAD_REQUEST);
        }
        try {
            const newObject = new this.registerModel(data);
            const response = await newObject.save();
            if (response) {
                const responseAccount = await this.accountServices.getAccount(response.idAccount);
                const account = responseAccount.data;
                var formula = account.balance;
                if (response.typeMovement == 1) {
                    formula = account.balance - (response.price * response.quantity);
                }
                else {
                    formula = account.balance + (response.price * response.quantity);
                }
                account.balance = formula;
                const accountEdit = await this.accountServices.updateAccount(account, response.idAccount);
                if (accountEdit.respuesta) {
                    return { data: response, respuesta: true };
                }
                else {
                    this.deleteRegister(response.id);
                    throw new common_1.HttpException("Registro no creado" + accountEdit.error, common_1.HttpStatus.UNPROCESSABLE_ENTITY);
                }
            }
            else {
                throw new common_1.HttpException("Registro no creado", common_1.HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }
        catch (error) {
            throw new common_1.HttpException("Petición no válida. " + error, common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async deleteRegister(id) {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /registers/update");
        console.log("data --> ", id);
        if (!id || id === "") {
            return { error: "Faltan datos", respuesta: false };
        }
        try {
            const deleteItem = await this.registerModel.findByIdAndDelete(id);
            if (deleteItem && deleteItem != null) {
                return { data: deleteItem, respuesta: true };
            }
            else {
                return { error: "Registro no válido", respuesta: false };
            }
        }
        catch (error) {
            return { error: "Petición no válida. " + error, respuesta: false };
        }
    }
    getStrDate(dateToParse) {
        const formatYmd = date => date.toISOString().slice(0, 10);
        return formatYmd(dateToParse);
    }
    getDateMonth(formateDte) {
        const sptDate = formateDte.split("-");
        const complemention = sptDate[0] + "-" + sptDate[1];
        const startdate = complemention + "-01";
        const enddate = complemention + "-31";
        return { $gte: startdate, $lte: enddate };
    }
    byDate(a, b) {
        return new Date(b.key).valueOf() - new Date(a.key).valueOf();
    }
};
RegistersService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, mongoose_1.InjectModel)('Register')),
    __metadata("design:paramtypes", [mongoose_2.Model,
        category_service_1.CategoryService,
        acount_service_1.AccountService])
], RegistersService);
exports.RegistersService = RegistersService;
//# sourceMappingURL=registers.service.js.map