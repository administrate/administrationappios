"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppModule = void 0;
const common_1 = require("@nestjs/common");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const registers_module_1 = require("./registers/registers.module");
const users_module_1 = require("./users/users.module");
const methods_pay_module_1 = require("./methods-pay/methods-pay.module");
const mongoose_1 = require("@nestjs/mongoose");
const users_registry_module_1 = require("./users.registry/users.registry.module");
const auth_module_1 = require("./auth/auth.module");
const category_module_1 = require("./categories/category.module");
const status_module_1 = require("./status/status.module");
const account_module_1 = require("./acounts/account.module");
const MongooseConfig = { useNewUrlParser: true, };
const MongooseConectionModule = mongoose_1.MongooseModule.forRoot('mongodb://localhost/administrationdb', MongooseConfig);
let AppModule = class AppModule {
};
AppModule = __decorate([
    (0, common_1.Module)({
        imports: [MongooseConectionModule, registers_module_1.RegistersModule, users_module_1.UsersModule, methods_pay_module_1.MethodsPayModule, users_registry_module_1.UsersRegistryModule, auth_module_1.AuthModule, category_module_1.CategoryModule, status_module_1.StatusModule, account_module_1.AccountsModule],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map