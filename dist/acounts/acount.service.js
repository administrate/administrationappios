"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_2 = require("mongoose");
let AccountService = class AccountService {
    constructor(accountsModel) {
        this.accountsModel = accountsModel;
    }
    async getAccounts() {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /accounts/");
        console.log("data --> NA");
        try {
            const response = await this.accountsModel.find();
            console.log("response --> ", response);
            return { data: response, respuesta: true };
        }
        catch (error) {
            console.log("response --> ", error);
            return { error: "Petición no válida. " + error, respuesta: false };
        }
    }
    async getAccount(id) {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /accounts/:id");
        console.log("data --> ", id);
        try {
            const response = await this.accountsModel.findById(id);
            console.log("response->", response);
            if (response && response != null) {
                return { data: response, respuesta: true };
            }
            else {
                return { error: "Cuenta no existente", respuesta: false };
            }
        }
        catch (error) {
            console.log("response --> ", error);
            return { error: "Petición no válida. " + error, respuesta: false };
        }
    }
    async getAccountsByUser(id) {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /accounts/byUser");
        console.log("data --> ", id);
        try {
            const response = await this.accountsModel.find({ idUser: id }).exec();
            console.log("response --> ", response);
            return { data: response, respuesta: true };
        }
        catch (error) {
            console.log("response --> ", error);
            return { error: "Petición no válida. " + error, respuesta: false };
        }
    }
    async createAccount(item) {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /accounts/create");
        console.log("data --> ", item);
        if (!('name' in item) || item.name == "" || !('idUser' in item) || item.idUser === "" || item.balance <= 0 || !("balance" in item)) {
            throw new common_1.HttpException('Faltan algunos datos obligatorios, favor de verificar.', common_1.HttpStatus.BAD_REQUEST);
        }
        try {
            const newObject = new this.accountsModel(item);
            const response = await newObject.save();
            console.log("response->", response);
            if (response) {
                return { data: response, respuesta: true };
            }
            else {
                throw new common_1.HttpException("Registro no creado", common_1.HttpStatus.UNPROCESSABLE_ENTITY);
            }
        }
        catch (error) {
            console.log("Response --> ", error);
            throw new common_1.HttpException("Petición no válida. " + error, common_1.HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    async deleteAccount(id) {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /accounts/delete");
        console.log("data --> ", id);
        if (!id || id === "") {
            return { error: "Faltan datos", respuesta: false };
        }
        try {
            const deleteItem = await this.accountsModel.findByIdAndDelete(id);
            console.log("response --> ", deleteItem);
            if (deleteItem && deleteItem != null) {
                return { data: deleteItem, respuesta: true };
            }
            else {
                return { error: "Cuenta no válida", respuesta: false };
            }
        }
        catch (error) {
            console.log("Response --> ", error);
            return { error: "Petición no válida. " + error, respuesta: false };
        }
    }
    async updateAccount(data, id) {
        console.log("\n -------------------------------------------------");
        console.log("service -->  /users/:id");
        console.log("id --> ", id);
        console.log("data --> ", data);
        try {
            if (id) {
                const account = await this.accountsModel.findById(id);
                if (account) {
                    console.log("Cuenta --->", account);
                    if ('balance' in data) {
                        account.balance = data.balance;
                        const response = await account.save();
                        if (response) {
                            return { respuesta: true, data: response };
                        }
                        else {
                            return { respuesta: false, error: 'No se pudo actualizar la cuenta' };
                        }
                    }
                    else {
                        return { respuesta: false, error: 'Dato [balance] es necesario.' };
                    }
                }
                else {
                    return { respuesta: false, error: 'Cuenta no existente.' };
                }
            }
        }
        catch (err) {
            return { respuesta: false, error: 'Error inesperado. [2000] ' + err };
        }
    }
};
AccountService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, mongoose_1.InjectModel)('Account')),
    __metadata("design:paramtypes", [typeof (_a = typeof mongoose_2.Model !== "undefined" && mongoose_2.Model) === "function" ? _a : Object])
], AccountService);
exports.AccountService = AccountService;
//# sourceMappingURL=acount.service.js.map