import { Account } from './interfaces/Account';
import { Model } from 'mongoose';
import { CreateAccountDto } from './dto/CreateAccountDto';
export declare class AccountService {
    private accountsModel;
    constructor(accountsModel: Model<Account>);
    getAccounts(): Promise<{}>;
    getAccount(id: string): Promise<{}>;
    getAccountsByUser(id: string): Promise<{}>;
    createAccount(item: Account): Promise<{}>;
    deleteAccount(id: string): Promise<{}>;
    updateAccount(data: CreateAccountDto, id: string): Promise<{}>;
}
