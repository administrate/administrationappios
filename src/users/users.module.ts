import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller'
import { MongooseModule } from '@nestjs/mongoose'
import { UserSchema } from './schemas/user.schema';
const SchemasModules = MongooseModule.forFeature([
  {name:'User', schema: UserSchema}
])

@Module({
  providers: [UsersService],
  imports: [SchemasModules],
  controllers: [UsersController],
  exports: [UsersService]
})
export class UsersModule {}
