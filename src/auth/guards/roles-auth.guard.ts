import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { Reflector } from '@nestjs/core';

import { ROLES_KEY } from '../decorators/roles.decorator';
import { PayloadToken } from 'src/models/token.model';
import { Role } from 'src/models/rol.model';

@Injectable()
export class RolesAuthGuard implements CanActivate {
  constructor(private reflector:Reflector){}

  canActivate(context: ExecutionContext,): boolean | Promise<boolean> | Observable<boolean> {
    const roles = this.reflector.get<Role[]>(ROLES_KEY, context.getHandler());
    if(!roles){
      return true
    }
    const rqt = context.switchToHttp().getRequest();
    const userModel = rqt.user as PayloadToken
    if(!userModel) return true
    
    const isAuth = roles.some((it) => it === userModel.role)
    if(!isAuth){
      throw new UnauthorizedException('Usuario no tiene permisos de administrador.')
    }
    return isAuth;
  }
}
