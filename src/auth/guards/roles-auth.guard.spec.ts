import { RolesAuthGuard } from './roles-auth.guard';

describe('RolesAuthGuard', () => {
  it('should be defined', () => {
    expect(new RolesAuthGuard()).toBeDefined();
  });
});
