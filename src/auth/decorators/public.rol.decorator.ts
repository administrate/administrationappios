import { SetMetadata } from "@nestjs/common";

export const IS_ROL_PUBLIC_KEY = 'isPublicRole'
export const PublicRol = () => SetMetadata(IS_ROL_PUBLIC_KEY, true);