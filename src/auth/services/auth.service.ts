import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt'
import { JwtService } from '@nestjs/jwt';

import { UsersService } from 'src/users/users.service';
import { User } from './../../users/interfaces/User';
import { PayloadToken } from 'src/models/token.model';

@Injectable()
export class AuthService {
    constructor(
        private usersServices:UsersService, 
        private jwtService:JwtService,
        ){}

    async validateUser(email:string, password:string){
        console.log("\n -------------------------------------------------");
        console.log("service -->  /auth/Login");
        console.log("data --> ", {user:email, password:password} );

        const user = await this.usersServices.getUserByMail(email)
        if(user.respuesta){
            const isMatch = await bcrypt.compare(password, user.data.password)
            if(isMatch){
                const {password, ...rta} = user.data.toJSON();
                return rta
            }
        }
        
        return null
    }

    generateJWT(user:User){
        const payload:PayloadToken = {role: user.type , sub: user._id};
        return {
            data:{
                accessToken: this.jwtService.sign(payload),
                user,
            },
            respuesta:true
        }
    }
}
