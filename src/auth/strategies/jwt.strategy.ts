import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import { SECRETKEY } from "src/models/enviroment";
import { PayloadToken } from "src/models/token.model";
@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt'){
    constructor(){
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            ignoreExpiration: false,
            secretOrKey:SECRETKEY
        });
    }

    validate(payload: PayloadToken){
        return payload;
    }
}