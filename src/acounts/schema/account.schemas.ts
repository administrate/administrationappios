import { Schema } from "mongoose"

export const AccountSchema = new Schema({
    balance: Number,
	idUser: {
		required: false,
		type:Schema.Types.ObjectId,
		default : null
	},
	name:String,
	createAt:{
		type:Date,
		default: Date.now
	},
	updateAt:{
		type:Date,
		default: Date.now
	}
})