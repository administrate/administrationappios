import { Test, TestingModule } from '@nestjs/testing';
import { Users.RegistryService } from './users.registry.service';

describe('Users.RegistryService', () => {
  let service: Users.RegistryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [Users.RegistryService],
    }).compile();

    service = module.get<Users.RegistryService>(Users.RegistryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
