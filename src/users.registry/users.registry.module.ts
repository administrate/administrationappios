import { Module } from '@nestjs/common';
import { UsersRegistryService } from './users.registry.service';
import { UsersRegistryController } from './users.registry.controller';

import { MongooseModule } from '@nestjs/mongoose'
import { UserRegistrySchema } from './schemas/user.registry.schema';

const SchemasModules = MongooseModule.forFeature([
  {name:'UserRegistry', schema: UserRegistrySchema}
])
@Module({
  imports:[SchemasModules],
  providers: [UsersRegistryService],
  controllers: [UsersRegistryController],
})
export class UsersRegistryModule {}
