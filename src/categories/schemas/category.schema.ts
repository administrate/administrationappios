
import { Schema } from "mongoose"

export const CategorySchema = new Schema({
    description: String,
	data: String,
	createAt:{
		type:Date,
		default: Date.now
	},
	updateAt:{
		type:Date,
		default: Date.now
	}
})

