import { Controller, Body, Delete, Get, Param, Post, UseGuards } from '@nestjs/common';
import { CategoryService } from './category.service';
import { Category } from './interfaces/Category';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';

@UseGuards(JwtAuthGuard)
@Controller('category')
export class CategoryController {
    constructor(private methodPayService:CategoryService){}
    @Get()
    getMethodsPay():{}{
        return this.methodPayService.getCategories();
    }

    @Post('create')
    createMethodPay(@Body() data:Category):{}{
        return this.methodPayService.createCategory(data);
    }

    @Delete(':id')
    deleteMethodPay(@Param('id') id):{}{
        return this.methodPayService.deleteCategory(id);
    }
}
