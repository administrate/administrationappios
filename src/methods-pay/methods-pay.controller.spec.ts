import { Test, TestingModule } from '@nestjs/testing';
import { MethodsPayController } from './methods-pay.controller';

describe('MethodsPayController', () => {
  let controller: MethodsPayController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MethodsPayController],
    }).compile();

    controller = module.get<MethodsPayController>(MethodsPayController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
