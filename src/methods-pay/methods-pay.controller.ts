import { Body, Controller, Delete, Get, Param, Post, UseGuards } from '@nestjs/common';

import { MethodPay } from './interfaces/MethodPay';
import { MethodsPayService } from './methods-pay.service';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';

@UseGuards(JwtAuthGuard)
@Controller('methods-pay')
export class MethodsPayController {

    constructor(private methodPayService:MethodsPayService){}
    @Get()
    getMethodsPay():{}{
        return this.methodPayService.getMethodsPay();
    }

    @Get(':id')
    getMethodPay(@Param('id') id):{}{
        return this.methodPayService.getMethodPay(id);
    }

    @Post('create')
    createMethodPay(@Body() data:MethodPay):{}{
        return this.methodPayService.createMethodPay(data);
    }

    @Delete(':id')
    deleteMethodPay(@Param('id') id):{}{
        return this.methodPayService.deleteMethodPay(id);
    }
}
