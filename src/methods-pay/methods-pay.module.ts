import { Module } from '@nestjs/common';
import { MethodsPayService } from './methods-pay.service';
import { MethodsPayController } from './methods-pay.controller';
import { MongooseModule } from '@nestjs/mongoose'
import { MethodPaySchema } from './schemas/methos-pay.schemas';

const SchemasModules = MongooseModule.forFeature([
  {name:'MethosPay', schema: MethodPaySchema}
])
@Module({
  providers: [MethodsPayService],
  imports:[SchemasModules],
  controllers:[MethodsPayController]
})
export class MethodsPayModule {}
