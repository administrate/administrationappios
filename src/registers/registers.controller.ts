import { Controller, Get, Post, Put, Delete, Param, Body, UseGuards, Query} from '@nestjs/common';

import {CreateRegisterDto} from './dto/CreateRegisterDto'
import {RegistersService} from './registers.service'
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';

@UseGuards(JwtAuthGuard)
@Controller('registers')
export class RegistersController {

    constructor(private registerService:RegistersService){}
    
    @Get()
    getRegisters():{}{
        return this.registerService.getRegisters();
    }
    
    @Get(':id')
    getRegister(@Param('id') id): {}{
        return this.registerService.getRegister(id);
    }

    @Get('byAccount/:id')
    getRegisterByUSer(@Param('id') id): {}{
        return this.registerService.getRegistersByAccount(id);
    }
    @Get('graphs/:id')
    getGraphsByUSer(@Param('id') id, @Query('filter') filter): {}{
        return this.registerService.getAnalithyscsByAccount(id, filter);
    }
    @Post()
    createRegister(@Body() data:CreateRegisterDto):{}{
        return this.registerService.createRegister(data);
    }
    @Delete(':id')
    deleteRegisters(@Param('id') id):{}{
        return this.registerService.deleteRegister(id)
    }
}
