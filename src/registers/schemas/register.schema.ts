
import { now, Schema } from "mongoose"

export const RegisterSchema = new Schema({
    name:String,
	observations: String,
	quantity:Number,
	typeMovement:Number,
	price:Number,
	ticket:String,
	place: String,
	payTypeId:{
		required: false,
		type:Schema.Types.ObjectId,
		default : null
	},
	tips: Number,
	idStatus:{
		required: false,
		type:Schema.Types.ObjectId,
		default : null
	},
	createAt:{
		type:Date,
		default: Date.now()
	},
	updateAt:{
		type:Date,
		default: Date.now()
	},
	idAccount:{
		required: false,
		type:Schema.Types.ObjectId,
		default : null
	},
	idCategory:{
		required: false,
		type:Schema.Types.ObjectId,
		default : null
	}
})

