import { Module } from '@nestjs/common';
import { RegistersService } from "./registers.service";
import { RegistersController } from "./registers.controller";
import { MongooseModule } from '@nestjs/mongoose'
import { RegisterSchema } from './schemas/register.schema';
import { CategoryModule } from 'src/categories/category.module';
import { AccountService } from 'src/acounts/acount.service';
import { AccountsModule } from 'src/acounts/account.module';

const SchemasModules = MongooseModule.forFeature([
    {name:'Register', schema: RegisterSchema}
  ])
@Module({
    imports: [SchemasModules, CategoryModule, AccountsModule],
    controllers: [RegistersController],
    providers: [RegistersService],
    exports:[RegistersService]
})
export class RegistersModule {}
